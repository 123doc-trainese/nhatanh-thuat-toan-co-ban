<?php

$words =  ["This", "is", "an", "example", "of", "text", "justification."];
$maxWidth = 16;

function test($words, $maxWidth)
{
    $i = 0;
    $j = 0;
    $len = 0;
    $result = [];

    // tách ký tự 1 dòng thành mảng
    while ($i < count($words) && $j < count($words)) {

        // độ dài ký tự 
        $len = $len + strlen($words[$i]);
        // dộ dài nhỏ hơn max
        if ($len <= $maxWidth) {
            // gán ký tự vào mảng
            $result[$j][] = $words[$i];
            $i++;
            // thêm khoảng trắng
            $len++;
        } else {
            $len = 0;
            $j++;
        }
    }


    $resultLen = count($result);
    for ($i = 0; $i < count($result); $i++) {
        // khoảng trắng còn thiếu để chuỗi đạt độ dài maxWidth
        $spaceLen = $maxWidth - strlen(implode('', $result[$i]));

        // số ký tự trong dòng $i
        $iResultLen = count($result[$i]);

        // có 1 ký tự
        if ($iResultLen == 1) {
            // thêm khoảng trắng còn thiếu vào bên phải
            $string[] = $result[$i][0] . makeSpaces($spaceLen);
            continue;
        }

        // dòng cuối
        if (($resultLen - 1) == $i) {
            // các từ cách nhau 1 khoảng trắng
            $lastStr = implode(' ', $result[$i]);
            // thêm khoảng trắng còn thiếu vào cuối chuỗi
            $string[] = $lastStr . makeSpaces($maxWidth - strlen($lastStr));
            continue;
        }

        // các dòng có nhiều hơn 1 từ
        // khoảng trắng giữa các từ
        $makeSpaceLen = $spaceLen / ($iResultLen - 1);

        // khoảng trắng giữa các từ là số nguyên
        if ($makeSpaceLen == intval($makeSpaceLen)) {
            // thêm khoảng trắng giữa các từ bằng nhau
            $spaceStr = makeSpaces($makeSpaceLen);
            $string[] = implode($spaceStr, $result[$i]);
            continue;
        }

        // khoảng trắng giữa các từ không bằng nhau
        $rowStr = '';

        // số ký tự trong 1 dòng 
        $iLen = $iResultLen;
        // làm tròn khoảng trắng
        $ceilMakeSpaceLen = ceil($makeSpaceLen);
        // duyệt từng từ trong
        for ($j = 0; $j < $iResultLen; $j++) {
            $rowStr .= $result[$i][$j];
            // khoảng trắng còn thiếu < khoảng trắng giữa các từ
            if ($spaceLen < $ceilMakeSpaceLen) continue;
            // thêm khoảng trắng
            $rowStr .= makeSpaces($ceilMakeSpaceLen);

            //tính lại khoảng trắng còn thiếu
            $spaceLen = $spaceLen - $ceilMakeSpaceLen;

            // giảm số từ trong mảng
            $iLen--;
            if ($iLen == 1) {
                $ceilMakeSpaceLen = 1;
            } else {
                // tính lại khoảng trắng giữa các từ
                $makeSpaceLen = $spaceLen / ($iLen - 1);
                // làm tròn 
                $ceilMakeSpaceLen = ceil($makeSpaceLen);
            }
        }
        $string[] = $rowStr;
    }
    return $string;
}

function makeSpaces($len, $i = 0, $res = '')
{
    while ($i++ < $len) {
        $res .= ' ';
    }
    return $res;
}

$result = test($words, $maxWidth);

print_r($result);