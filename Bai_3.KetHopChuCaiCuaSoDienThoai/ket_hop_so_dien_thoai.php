<?php
$arr = [2 => ['a', 'b', 'c'], 3 => ['d', 'e', 'f'], 4 => ['g', 'h', 'i'], 5 => ['j', 'k', 'l'], 6 => ['m', 'n', 'o'], 7 => ['p', 'q', 'r', 's'], 8 => ['t', 'u', 'v'], 9 => ['w', 'y', 'y']];
$a = "2347";
$result = [];
$s = '';
function test($arr, $a, $result)
{
    // chuỗi số nhập = 0 => chuỗi rỗng
    if (strlen($a) == 0) {
        return $result = [];
    }
    // nhập 1 số => in ra phần tử ứng với số đó
    if (strlen($a) == 1) {
        // chuyển chuỗi thành số
        $s = intval($a);
        return $result = $arr[$s];
    }

    // nhiều chuỗi hơn 1 số
    // duyệt từng số
    for ($i = 0; $i < strlen($a); $i++) {
        $s = intval($a[$i]);

        // mảng $result rỗng thì bằng bảng số đầu
        if (count($result) == 0) {
            $result = $arr[$s];

            // mảng $result đã có dữ liệu 
        } else {
            $result1 = [];

            //duyệt $result 
            for ($j = 0; $j < count($result); $j++) {

                // duyệt mảng ký tự
                for ($k = 0; $k < count($arr[$s]); $k++) {
                    // nối từng ký tự $result với mảng ký tự số
                    $result1[] = $result[$j] . $arr[$s][$k];
                }
            }
            $result = $result1;
        }
    }
    return $result;
}

// $a = test($arr, $a, $result);
// print_r($a);

function numberCombination($i)
{
    global $arr, $a, $s, $result;
    if ($a == '') return [];
    $val = $a[$i];
    // echo $val;
    for ($j = 0; $j < count($arr[$val]); $j++) {
        $s .= $arr[$val][$j];
        if ($i == strlen($a) - 1) array_push($result, $s);
        else numberCombination($i + 1);
        $s = substr($s, 0, -1);
    }
}

numberCombination(0);
print_r($result);