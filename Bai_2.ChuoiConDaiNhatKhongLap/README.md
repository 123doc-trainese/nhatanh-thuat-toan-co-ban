# Bài 2: Chuỗi con dài nhất không có ký tự lặp lại
## Đề bài
Cho một chuỗi `s`, tìm độ dài của *chuỗi con dài nhất* không có ký tự lặp lại.
### Ví dụ 1:
```
Đầu vào: s = "abcabcbb"
Đầu ra: 3
Giải thích: Câu trả lời là "abc", với độ dài là 3.
```
### Ví dụ 2:
```
Đầu vào: s = "bbbbb"
Đầu ra: 1
Giải thích: Câu trả lời là "b", với độ dài là 1.
```
### Ví dụ 3:
```
Đầu vào: s = "pwwkew"
Đầu ra: 3
Giải thích: Câu trả lời là "wke", với độ dài là 3.
Lưu ý rằng câu trả lời phải là một chuỗi con, "pwke" là một dãy con chứ không phải một chuỗi con.
```

### Điều kiện:
- 0 <=` s.length` <= 5 * 104
- `s` bao gồm các chữ cái tiếng Anh, chữ số, ký hiệu và dấu cách.

## Hướng giải quyết
 - duyệt chuỗi đến khi gặp phần tử lặp.
    - gán max là độ dài chuỗi
 - tăng index lên 1 rồi duyệt tiếp.
    - so sánh độ dài chuỗi mới với max. 
 - trả vể max là độ dài chuỗi con dài nhất
