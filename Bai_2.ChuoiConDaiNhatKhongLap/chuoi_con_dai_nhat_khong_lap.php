<?php
$s = "abcabcbb";
$result = '';
$i = 0;
$j = 0;
$max = 0;

while ($j < strlen($s) && $i < strlen($s)) {
    // không tìm thấy $s[$j] trong $result
    if (strpos($result, $s[$j]) == '') {
        // thêm $s[$j] vào $result
        $result = $result . $s[$j];
        // tính max
        $max = ($j - $i + 1) > $max ? ($j - $i + 1) : $max;
        $j++;
    } else {
        // nếu có phần tử lặp. loại phần tử đầu khỏi chuỗi r duyệt
        $result = substr($result, 1);
        $i++;
    }
}

echo $max . "\n";