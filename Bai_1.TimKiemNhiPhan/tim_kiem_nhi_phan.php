<?php
// tìm kiếm nhị phân
$arr = [5, 7, 7, 8, 8, 10];
$target = 8;

function first($arr, $target)
// tìm vị trí bắt đầu 
{

    $left = 0;
    $right = count($arr) - 1;
    // vị trí bắt đầu = -1
    $l = -1;
    while ($left <= $right) {
        // tìm vị trí ở giữa
        $mid = floor(($left + $right) / 2);
        if ($arr[$mid] == $target) {
            // mid = target gán vị trí đầu = mid
            $l = $mid;
            // tìm vị trí đầu của target
            $right = $mid - 1;
        } else if ($arr[$mid] > $target) {
            // mid > target => giảm right để tìm tiếp
            $right = $mid - 1;
        } else {
            // mid < target; left = mid + 1 để tìm tiếp
            $left = $mid + 1;
        }
    }
    return $l;
}

function last($arr, $target)
{
    // tìm vị trí cuối cùng
    $left = 0;
    $right = count($arr) - 1;
    $r = -1;
    while ($left <= $right) {

        $mid = floor(($left + $right) / 2);

        if ($arr[$mid] == $target) {
            $r = $mid;
            $left = $mid + 1;
        } else if ($arr[$mid] > $target) {
            $right = $mid - 1;
        } else {
            $left = $mid + 1;
        }
    }
    return $r;
}

$result[] = first($arr, $target);
$result[] = last($arr, $target);
print_r($result);