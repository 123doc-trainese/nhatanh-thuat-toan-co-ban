# Tìm kiếm nhị phân
## Để bài
Cho một mảng các số nguyên `nums` được sắp xếp theo thứ tự không giảm, hãy tìm vị trí bắt đầu và kết thúc của một giá trị` target` nhất định.
Nếu không tìm thấy `target` trong mảng, hãy trả về` [-1, -1] `.

Bạn phải viết một thuật toán với độ phức tạp thời gian chạy `O (log n)`.

### Ví dụ 1:
```
Đầu vào: nums = [5,7,7,8,8,10], target = 8
Đầu ra: [3,4]
```
### Ví dụ 2:
```
Đầu vào: nums = [5,7,7,8,8,10], target = 6
Đầu ra: [-1, -1]
```
### Ví dụ 3:
```
Đầu vào: nums = [], target = 0
Đầu ra: [-1, -1]
```
### Điều kiện
- 0 <= `nums.length` <= 105
- -109 <= `nums [i]` <= 109
- `nums` là một mảng không giảm.
- -109 <= `target` <= 109
## Hướng giải 
    - tìm vị trí đầu target
        - vị trí đầu = -1 nếu tìm thấy target gán vị trí đầu = vị trí target
            - tìm tiếp từ đầu đến vị trí target -1
        - nếu target > mid gán left = mid +1;
        - nếu target <  mid gán right = mid +1;
    - tìm vị trí cuối target
        - vị trí cuối = -1 nếu tìm thấy target gán vị trí cuối = vị trí target
            - tìm tiếp từ vị trí target + 1 đến cuối
        - nếu target > mid gán left = mid +1;
        - nếu target <  mid gán right = mid +1;